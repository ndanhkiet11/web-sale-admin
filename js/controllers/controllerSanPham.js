export let onLoading = () => {
  document.getElementById("loading").style.display = "flex";
};
export let offLoading = () => {
  document.getElementById("loading").style.display = "none";

};
export let resetForm = () => {
  document.getElementById("productForm").reset();
};
export let disabledInput = (idSP) => {
  document.getElementById(idSP).disabled = true;
};
export let enabledInput = (idSP) => {
  document.getElementById(idSP).disabled = false;
};

export let renderThongTinSanPham = (list) => {
  let contentHTML = "";
  list.forEach((sanPham) => {
    contentHTML += `<tr>
        <td>${sanPham.id}</td>
        <td>${sanPham.ten}</td>
        <td>${sanPham.gia}</td>
        <td><img class="img-fluid" src="${sanPham.hinhAnh}"></td>
        <td>
        <p>"${sanPham.moTa}"</p>
        <p>Màn Hình: ${sanPham.manHinh}</p>
        <p>Camera Sau: ${sanPham.backCamera}</p>
        <p>Camera Trước: ${sanPham.frontCamera}</p>
       </td>
        <td>
        <button class="btn btn-primary m-1" onclick="suaSanPham(${sanPham.id})" data-toggle="modal"
          data-target="#myModal">Sửa</button>
          <button class="btn btn-danger m-1" onclick="xoaSanPham(${sanPham.id})">Xoá</button>
        </td>
        </tr>`;
  });
  document.getElementById("tableDanhSachSP").innerHTML = contentHTML;
};

export let layThongTinTuForm = () => {
  let id = document.getElementById("idSP").value.trim();
  let loai = document.getElementById("loaiSP").value.trim();
  let ten = document.getElementById("tenSP").value.trim();
  let gia = document.getElementById("giaSP").value.trim();
  let manHinh = document.getElementById("manHinh").value.trim();
  let frontCamera = document.getElementById("frontCamera").value.trim();
  let backCamera = document.getElementById("backCamera").value.trim();
  let hinhAnh = document.getElementById("hinhSP").value.trim();
  let moTa = document.getElementById("moTa").value.trim();

  return {
    id: id,
    name: ten,
    price: gia,
    screen: manHinh,
    backCamera: backCamera,
    frontCamera: frontCamera,
    img: hinhAnh,
    desc: moTa,
    type: loai,
  };
};

export let showThongTinLenForm = (sanPham) => {
  document.getElementById("idSP").value = sanPham.id;
  document.getElementById("loaiSP").value = sanPham.type;
  document.getElementById("tenSP").value = sanPham.name;
  document.getElementById("giaSP").value = sanPham.price;
  document.getElementById("manHinh").value = sanPham.screen;
  document.getElementById("frontCamera").value = sanPham.frontCamera;
  document.getElementById("backCamera").value = sanPham.backCamera;
  document.getElementById("hinhSP").value = sanPham.img;
  document.getElementById("moTa").value = sanPham.desc;
};

//Kiem tra rong
export let kiemTraRong = (value, idError) => {
  if (value.length == 0) {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText =
      "Trường này không được rỗng";
    return false;
  } else {
    document.getElementById(idError).style.display = "block";
    document.getElementById(idError).innerText = "";
    return true;
  }
};

//reset thong bao
export let resetxuatThongBao=()=> {
    var spanThongbao = document.querySelectorAll(".invalid-feedback");
    for (var i = 0; i < spanThongbao.length; i++) {
      spanThongbao[i].innerText = "";
      spanThongbao[i].style.display = "none";
    }
  }
