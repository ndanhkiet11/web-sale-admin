let BASE_URL = "https://633ec04e83f50e9ba3b76077.mockapi.io/";
export let getListServ = () => {
  return axios({
    url: `${BASE_URL}/web-sale`,
    method: "GET",
  });
};

export let postCreateProductServ = (product) => {
  return axios({
    url: `${BASE_URL}/web-sale`,
    method: "POST",
    data: product,
  });
};

export let deleteProductByIdServ = (id) => {
  return axios({
    url: `${BASE_URL}/web-sale/${id}`,
    method: "DELETE",
  });
};

export let getProductServ = (id) => {
  return axios({
    url: `${BASE_URL}/web-sale/${id}`,
    method: "GET",
  });
};

export let putProductServ = (sanPham) => {
  return axios({
    url: `${BASE_URL}/web-sale/${sanPham.id}`,
    method: "PUT",
    data: sanPham,
  });
};
