import {
  getListServ,
  postCreateProductServ,
  deleteProductByIdServ,
  getProductServ,
  putProductServ,
} from "./services/sanPhamService.js";
import { SanPham } from "./models/modelSanPham.js";
import {
  offLoading,
  onLoading,
  resetForm,
  disabledInput,
  enabledInput,
  renderThongTinSanPham,
  layThongTinTuForm,
  showThongTinLenForm,
  kiemTraRong,
  resetxuatThongBao,
} from "./controllers/controllerSanPham.js";

//load data SP
let renderListProductsServ = () => {
  onLoading();
  getListServ()
    .then((res) => {
      let danhSachSanPham = res.data.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      renderThongTinSanPham(danhSachSanPham);
      offLoading();
    })
    .catch((err) => {
      Swal.fire("Load danh sách Sản phẩm thất bại!");
      offLoading();
    });
};
renderListProductsServ();
//reset form
document.getElementById("btnThem").onclick = () => {
  resetForm();
  disabledInput("capNhatSP");
  enabledInput("themSP");
  resetxuatThongBao();
};

//them thong tin san pham
let themSanPham = () => {
  onLoading();
  let dataForm = layThongTinTuForm();
  let isValid = true;
  isValid &=
    kiemTraRong(dataForm.type, "invalidLoai") &
    kiemTraRong(dataForm.name, "invalidTen") &
    kiemTraRong(dataForm.price, "invalidGia") &
    kiemTraRong(dataForm.screen, "invalidManHinh") &
    kiemTraRong(dataForm.frontCamera, "invalidFrontCamera") &
    kiemTraRong(dataForm.backCamera, "invalidBackCamera") &
    kiemTraRong(dataForm.img, "invalidHinhAnh") &
    kiemTraRong(dataForm.desc, "invalidMoTa");
  if (!isValid) {
    offLoading();
    // return Swal.fire("Thêm Sản phẩm thất bại!");
    return;
  }
  postCreateProductServ(dataForm)
    .then((res) => {
      renderListProductsServ();
      Swal.fire("Thêm Sản phẩm thành công!");
      offLoading();
    })
    .catch((err) => {
      Swal.fire("Thêm Sản phẩm thất bại!");
      offLoading();
    });

  resetForm();
  resetxuatThongBao();
};
window.themSanPham = themSanPham;

// xoa thong tin san pham
let xoaSanPham = (id) => {
  onLoading();
  deleteProductByIdServ(id)
    .then((res) => {
      renderListProductsServ();
      Swal.fire("Xoá Sản phẩm thành công!");
      offLoading();
    })
    .catch((err) => {
      Swal.fire("Xoá Sản phẩm thất bại!");
      offLoading();
    });
};
window.xoaSanPham = xoaSanPham;

//sua thong tin san pham
let suaSanPham = (id) => {
  resetxuatThongBao();
  enabledInput("capNhatSP");
  disabledInput("themSP");
  getProductServ(id)
    .then((res) => {
      showThongTinLenForm(res.data);
    })
    .catch((err) => {
      Swal.fire("Không tìm thấy thông tin sản phẩm!");
    });
};
window.suaSanPham = suaSanPham;

// cap nhat san pham
let capNhatSanPham = () => {
  let spEdit = layThongTinTuForm();
  let isValid = true;
  isValid &=
    kiemTraRong(spEdit.type, "invalidLoai") &
    kiemTraRong(spEdit.name, "invalidTen") &
    kiemTraRong(spEdit.price, "invalidGia") &
    kiemTraRong(spEdit.screen, "invalidManHinh") &
    kiemTraRong(spEdit.frontCamera, "invalidFrontCamera") &
    kiemTraRong(spEdit.backCamera, "invalidBackCamera") &
    kiemTraRong(spEdit.img, "invalidHinhAnh") &
    kiemTraRong(spEdit.desc, "invalidMoTa");
  if (!isValid) {
    return Swal.fire("Cập nhật Sản phẩm thất bại!");
  }
  putProductServ(spEdit)
    .then((res) => {
      renderListProductsServ();
      Swal.fire("Cập nhật Sản phẩm thành công!");
      resetForm();
      enabledInput("themSP");
      disabledInput("capNhatSP");
    })
    .catch((err) => {
      Swal.fire("Cập nhật Sản phẩm thất bại!");
    });

  resetxuatThongBao();
};
window.capNhatSanPham = capNhatSanPham;

//tim ten san pham
document.getElementById("btnTimSP").onclick = () => {
  onLoading();
  let tenSP = document.getElementById("txtSanPham").value;
  getListServ()
    .then((res) => {
      let timSanPham = res.data.filter((sanPham) => {
        return sanPham.name.toLowerCase().indexOf(tenSP.toLowerCase()) > -1;
      });
      if (timSanPham.length == 0) {
        offLoading();
        return Swal.fire("Không tìm thấy Sản Phẩm!");
      }
      let danhSachSanPham = timSanPham.map((item) => {
        return new SanPham(
          item.id,
          item.name,
          item.price,
          item.screen,
          item.backCamera,
          item.frontCamera,
          item.img,
          item.desc,
          item.type
        );
      });
      offLoading();
      renderThongTinSanPham(danhSachSanPham);
    })
    .catch((err) => {
      offLoading();
      Swal.fire("Lấy dữ liệu sản phẩm thất bại!");
    });
};
